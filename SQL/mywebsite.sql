﻿CREATE DATABASE mywebsite DEFAULT CHARACTER SET utf8;
USE mywebsite;

CREATE TABLE user(user_id int primary key auto_increment,login_id varchar(255),
                    user_name varchar(255),password varchar(255),
                    birth_date date,user_create_date datetime,
                    update_date datetime);

CREATE TABLE content(content_id int primary key auto_increment,user_id int,
                        category varchar(255),game varchar(255),
                        title varchar(255),body varchar(20000),
                        content_create_date datetime);

CREATE TABLE comment_table(comment_table_id int primary key auto_increment,content_id int,
                            user_id int,comment varchar(20000));
